<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian_model extends CI_Model
{
    //panggil nama table
    private $_table_header = "pembelian_header";
    private $_table_detail = "pembelian_detail";

    public function tampilDataPembelian()
    {
        $query	= $this->db->query(
            "SELECT * FROM " . $this->_table_header . " WHERE flag = 1"
        );
        return $query->result();	
    }

    public function savePembelianHeader()
    {
        $data['no_transaksi']   = $this->input->post('no_transaksi');
        $data['kode_supplier']  = $this->input->post('kode_supplier');
        $data['tanggal']        = date('Y-m-d');
        $data['approved']       = 1;
        $data['flag']           = 1;

        $this->db->insert($this->_table_header, $data);
    }

    public function idTransaksiTerakhir()
    {
        $query	= $this->db->query(
            "SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY id_pembelian_h DESC LIMIT 0,1"
        );
        $data_id = $query->result();

        foreach ($data_id as $data) {
            $last_id = $data->id_pembelian_h;
        }

        return $last_id;
    }

    public function tampilDataPembelianDetail($id)
    {
        $query	= $this->db->query(
            "SELECT A.*, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = 1 AND A.id_pembelian_h = '$id'"
        );  
        return $query->result();	
    }

    public function savePembelianDetail($id)
    {
        $qty    = $this->input->post('qty');
        $harga  = $this->input->post('harga');

        $data['id_pembelian_h'] = $id;
        $data['kode_barang']    = $this->input->post('kode_barang');
        $data['qty']            = $qty;
        $data['harga']          = $harga;
        $data['jumlah']         = $qty * $harga;
        $data['flag']           = 1;

        $this->db->insert($this->_table_detail, $data);
    }
	
	public function delete($id_pembelian_h)
	{
		$this->db->where('id_pembelian_h', $id_pembelian_h);
		$this->db->delete($this->_table_header);
		
	}


    public function rules()
    
    {
        return [
        [
        'field' => 'no_transaksi',
        'label' => 'no transaksi',
        'rules' => 'required|max_length[5]',
        'errors' => [
           'required' => 'no transaksi tidak boleh kosong.',
           'max_length' => 'no transaksi tidak boleh lebih dari 5 karakter.',
           ]

           ],
           [

         'field' => 'kode_supplier',
         'label' => 'kode supplier',
         'rules' => 'required',
         'errors' => [
           'required' => 'kode supplier tidak boleh kosong.',
            
                   ]
           ]
           ];
         }

         
     public function rules1()
    
    {
        return [
        [
        'field' => 'kode_barang',
        'label' => 'kode barang',
        'rules' => 'required|max_length[5]',
        'errors' => [
           'required' => 'kode barang tidak boleh kosong.',
           'max_length' => 'kode barang tidak boleh lebih dari 5 karakter.',
           ]
           ],
           [
         'field' => 'qty',
         'label' => 'qty',
         'rules' => 'required|numeric',
         'errors' => [
           'required' => 'qty tidak boleh kosong.',
           'numeric' => 'qty harus angka.',
            ]
           ],
           [
             'field' => 'harga',
         'label' => 'harga',
         'rules' => 'required|numeric',
         'errors' => [
           'required' => 'harga tidak boleh kosong.',
           'numeric' => 'harga harus angka.',
            
                   ]
           ]
           ];
           


    }
	
	public function tampilreportpembelian($tgl_awal, $tgl_akhir)
	{
  $tgl_awal=$this->input->post('tgl_awal');
  $tgl_akhir=$this->input->post('tgl_akhir');
	$this->db->select("A.id_pembelian_h,A.no_transaksi,A.tanggal,COUNT(B.kode_barang)as total_barang, SUM(B.qty) as total_qty,SUM(B.jumlah) as total_pembelian"); 
	$this->db->from("pembelian_header A");
	$this->db->join("pembelian_detail B","A.id_pembelian_h = B.id_pembelian_h");
	$this->db->where("A.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");
	$this->db->group_by("A.id_pembelian_h");
	$query = $this->db->get();
	
	return $query->result();
	}
	

}