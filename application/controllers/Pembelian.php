<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("pembelian_model");
		$this->load->model("supplier_model");
		$this->load->model("barang_model");
		$this->load->library('pdf/pdf');
		
		//load validasi
		$this->load->library('form_validation');
		
			// cek login akses
		$user_login = $this->session->userdata();
		if (count($user_login) <= 1) {
			redirect("Auth/index", "refresh");
	}
		
	}

	public function index()
	{
		$this->listPembelian();
    }
    
    public function listPembelian()
	{
		$data['data_pembelian'] = $this->pembelian_model->tampilDataPembelian();
        
		$data['content'] = 'forms/list_pembelian';
		$this->load->view('home2', $data);
    }
    
    public function input()
	{
      
        $data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
        
        
        //if (!empty($_REQUEST)) {
           // $pembelian_header = $this->pembelian_model;
           // $pembelian_header->savePembelianHeader();
            //$id_terakhir = array();
            //panggil ID transaksi terakhir
            //$id_terakhir = $pembelian_header->idTransaksiTerakhir();
           
			//redirect("pembelian/inputDetail/" . $id_terakhir, "refresh");
        //}
        $validation = $this->form_validation;
		$validation->set_rules($this->pembelian_model->rules());
		
		if ($validation->run()) {
			$this->pembelian_model->savePembelianHeader();
			$this->session->set_flashdata('info', '<div style="color : green">simpan data berhasil !</div>');
			redirect("pembelian/index", "refresh");
		}
			
		//$this->load->view('input_supplier');
		$data['content'] = 'forms/input_pembelian_header';
		$this->load->view('home2', $data);
        
		
	}
    public function inputDetail($id_pembelian_header)
	{
        // panggil data barang untuk kebutuhan form input
		 $data['id_header'] = $id_pembelian_header;
         $data['data_barang'] = $this->barang_model->tampilDataBarang();
         $data['data_pembelian_detail'] = $this->pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
        
        //if (!empty($_REQUEST)) {
          
            //$this->pembelian_model->savePembelianDetail($id_pembelian_header);
            
          
            //$kode_barang  = $this->input->post('kode_barang');
            //$qty        = $this->input->post('qty');
            //$this->barang_model->updateStok($kode_barang, $qty);

			//redirect("pembelian/inputDetail/" . $id_pembelian_header, "refresh");
        //}
         $validation = $this->form_validation;
		$validation->set_rules($this->pembelian_model->rules1());
		
		if ($validation->run()) {
			$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			$this->session->set_flashdata('info', '<div style="color : green">simpan data berhasil !</div>');
			redirect("pembelian/inputDetail/" . $id_pembelian_header, "refresh");
		}
			
		//$this->load->view('input_supplier');
		$data['content'] = 'forms/input_pembelian_detail';
		$this->load->view('home2', $data);
        
		
		//$this->load->view('input_pembelian_detail', $data);
	}
	
	public function deletepembelian($id_pembelian_h)
	{
		$m_pembelian = $this->pembelian_model;
		$m_pembelian->delete($id_pembelian_h);
		redirect("Pembelian/index", "refresh");
		
	}
	
	public function report(){
		$data['content'] = 'forms/report';
		$this->load->view('home2', $data);
		
	}
	
	public function report_pembelian(){
		$tgl_awal=$this->input->post('tgl_awal');
		/*$pisah=explode('/',$tgl_awal);
		$array=array($pisah[2],$pisah[0],$pisah[1]);
		$tgl_awal=implode('-', $array);*/
		
		$tgl_akhir=$this->input->post('tgl_akhir');
		/*$pisah=explode('/',$tgl_akhir);
		$array=array($pisah[2],$pisah[0],$pisah[1]);
		$tgl_akhir=implode('-', $array);*/
		
		$data['tgl_awal']=$this->input->post('tgl_awal');
		$data['tgl_akhir']=$this->input->post('tgl_akhir');
		$data['data_pembelian']  = $this->pembelian_model->tampilreportpembelian($tgl_awal, $tgl_akhir);
		$data['content'] = 'forms/form_pembelian';
		$this->load->view('home2', $data);
		
	}
	
	public function carilaporan()
	{
		if(!empty($_REQUEST)){
			//ambil proses tanggal
			$tgl_awal = $this->input->post('tgl_awal');
			$tgl_akhir = $this->input->post('tgl_akhir');
			$data['data_cari_pembelian'] = $this->pembelian_model->tampilDataPembelian
			($tgl_awal, $tgl_akhir);
			$data['tgl_awal'] = $tgl_awal;
			$data['tgl_akhir'] = $tgl_akhir;
			/*print_r($tgl_awal);die();*/
			
			$data['content'] = 'forms/list_pembelian';
			$this->load->view('home2', $data);
	}else{
		redirect("Pembelian/report/", "refresh");
	}
	
	}
	public function cetak($tgl_awal,$tgl_akhir)
	{
			
		$pdf = new FPDF('P','mm','A4');
		$pdf->AddPage();
		$pdf->SetFont('Arial', 'B', 15);
		$pdf->Cell(187, 7, 'Laporan Data Pembelian', 0, 1, 'C');
		$pdf->SetFont('Arial', 'B', 15);
		$pdf->Cell(190, 7, 'Toko Jaya Abadi Subchan', 0, 1, 'C');

		$pdf->Cell(10, 10,'',0,1,'L');
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(18, 6, 'No',1, 0, 'C');
		$pdf->Cell(22,6,'Id Pembelian',1,0,'C');
		$pdf->Cell(35,6,'Nomor Transaksi',1,0,'C');
		$pdf->Cell(30,6,'Tanggal',1,0,'C');

		$pdf->Cell(30,6,'Total Barang',1,0,'C');
		$pdf->Cell(30,6,'Total Qty',1,0,'C');
		$pdf->Cell(29,6,'Jumlah Nominal',1,1,'C');

		$pdf->SetFont('Arial', 'B', 10);
			$no = 0;
			$total = 0;
			$data_pembelian = $this->pembelian_model->tampilreportpembelian($tgl_awal,$tgl_akhir);
		foreach ($data_pembelian as $data) {
			$no ++;
			$pdf->Cell(18,6,$no,1,0,'C');
			$pdf->Cell(22,6,$data->id_pembelian_h,1,0,'C');
			$pdf->Cell(35,6,$data->no_transaksi,1,0,'C');
			$pdf->Cell(30,6,$data->tanggal,1,0,'C');
			$pdf->Cell(30,6,$data->total_barang,1,0,'C');
			$pdf->Cell(30,6,$data->total_qty,1,0,'C');
			$pdf->Cell(29,6,'Rp.'.number_format($data->total_pembelian),1,1,'R');

			$total += $data->total_pembelian;
		}
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(165,6,'Total Keseluruhan',1,0,'C');
		$pdf->Cell(29,6,'Rp.'.number_format($total),1,1,'R');
		$pdf->Output();
	}

}